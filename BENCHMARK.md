# Comparação

## Sequelize

### Vantagens

- Maior tempo de mercado
- Já é conhecido pela maior parte dos desenvolvedores
- É pouco "opinado", pois é possível fazer a mesma coisa de mais de uma maneira

### Desvantagens

- Sintaxe relativamente verbosa
- Documentação confusa
- Erros que, em uma quantidade considerável de vezes, são pouco explicativos
- Falta de integração com o TypeScript

## Prisma

### Vantagens

- Sintaxe enxuta
- Documentação simples de entender e bem exemplificada
- Erros que "vão direto ao ponto", mas sem ocultar detalhes
- Integração com MongoDB (experimental)
- Inferência automática da tipagem dos models
- Flexibilidade de uso entre JavaScript e TypeScript
- Fácil migração a partir de um banco já existente (cria os models automaticamente)
- Prisma Studio (visualizar o banco durante o desenvolvimento a partir da própria ferramenta)

### Desvantagens

- Pouco tempo de mercado
- Não é tão popular entre os desenvolvedores
- Leve barreira de entrada (é necessário aprender a sintaxe dos models)
- Models criados num único arquivo
- Por ser novo, ainda carece de algumas features
