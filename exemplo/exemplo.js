const { PrismaClient } = require("@prisma/client");

const connection = new PrismaClient();

await connection.sectors.findMany(
    {
        where: {
            id: 1
        },

        include: {
            rule: true
        }
    }
);

await connection.$executeRaw("SELECT * FROM users;");

const [create, unique, many] = await connection.$transaction([
    connection.sectors.create(),
    connection.sectors.findUnique(),
    connection.sectors.findMany()
])