
const { Model, DataTypes } = require("sequelize");

class Role extends Model {
    static init(connection) {
        super.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    autoIncrement: true,
                    primaryKey: true
                },

                name: {
                    type: DataTypes.STRING
                },

                guard_name: {
                    type: DataTypes.STRING
                },

                created_at: DataTypes.DATE,
                updated_at: DataTypes.DATE,
                deleted_at: DataTypes.DATE
            },
            {
                sequelize: connection,
                tableName: "roles",
                freezeTableName: true,
                updatedAt: "updated_at",
                createdAt: "created_at"
            }
        );
    }

    static associate(models) {}
}

module.exports = Role;