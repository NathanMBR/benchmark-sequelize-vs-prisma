'use strict';

const tableName = "roles";

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable(
      tableName,
      {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },

        name: {
          type: Sequelize.STRING(255),
          allowNull: false
        },

        guard_name: {
          type: Sequelize.STRING(255),
          allowNull: false
        },

        created_at: {
          type: Sequelize.DATE,
          allowNull: true
        },

        updated_at: {
          type: Sequelize.DATE,
          allowNull: true
        },

        deleted_at: {
          type: Sequelize.DATE,
          allowNull: true
        }
      }
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable(tableName);
  }
};
