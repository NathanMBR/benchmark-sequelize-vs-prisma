const prisma = require("@prisma/client")
const { PrismaClient } = prisma;

const connection = new PrismaClient();
module.exports = connection;