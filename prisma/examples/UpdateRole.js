const connection = require("../prisma/connection");

const role = await connection.role.update({
    where: {
        id: "id"
    },
    data: {
        name: "Updated name",
        guard_name: "Updated guard name"
    }
});